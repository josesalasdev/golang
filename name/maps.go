package name

func GetMap() map[string]int {
	// key: value
	mapTest := make(map[string]int)
	mapTest["first"] = 1
	mapTest["second"] = 2
	return mapTest
}
