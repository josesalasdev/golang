package main

import (
	"fmt"
	"strings"

	// local package
	"./name"
)

const unique string = "I am unique"

type Vertex struct {
	X int
	Y int
}

func main() {
	// function main
	vars()
	getVariables()
	sum(10, 11)
	getInteger()
	getStr()
	getArrayFix()
	getIf()
	getFor()
	getStructure()
	getPunter()
	name.GetNumber()
	name.GetMap()

}

func vars() (string, int, string) {
	// Go vars

	// bool

	// string

	// int  int8  int16  int32  int64
	// uint uint8 uint16 uint32 uint64 uintptr

	// byte // alias for uint8

	// rune // alias for int32
	// 	// represents a Unicode code point

	// float32 float64

	// complex64 complex128

	var message string
	message = "Hello World"
	vars := "status"
	fmt.Printf("> %s", message)
	fmt.Println(".")
	var (
		a = 2
		b = "1"
	)

	return b, a, vars
}

func getVariables() (int, int, int) {
	return 1, 2, 3
}

func sum(a int, b int) int {
	return a + b
}

func getInteger() (int, int32, int64) {
	return 1, 1000000000, 1000000000000000
}

func getFloat() (float32, float64) {
	return float32(0.10), float64(0.1)
}

func getStr() string {
	hello := string("Hello"[0])
	fmt.Println(hello)
	return hello

}

func getArrayFix() [3]int {
	var list [2]int
	list[0] = 1
	list[1] = 2
	fmt.Println(list)
	listTwo := [3]int{1, 2, 3}
	return listTwo
}

func getSlice() []string {
	var list []string
	list = append(list, "one")
	return list
}

func getIf() {
	var con string = "hello"
	if string(con[0]) == "h" {
		fmt.Print("string h")
	} else {
		fmt.Print("not h")
	}

}

func getFor() {
	for i := 0; i < 5; i++ {
		fmt.Println("For ", i)
	}
}

func getStringLib() {
	var text string = "in the park"
	strings.ToUpper(text)
}

func getStructure() {
	v := Vertex{}
	v.X = 4
	//v.Y = 8
	fmt.Println(v.X)
}

func getPunter() {
	p := Vertex{1, 2}
	q := &p
	q.X = 1e9
	fmt.Println(p)
}
